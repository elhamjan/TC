<?php
$targetDirectory = "./"; // 设定上传文件存储目录为当前目录

$targetFile = $targetDirectory . basename($_FILES["fileToUpload"]["name"]); // 获取上传文件名

$uploadOk = 1; // 用来标识上传是否成功

// 检查文件是否已存在
if (file_exists($targetFile)) {
    echo "该文件已存在.";
    $uploadOk = 0;
}

// 检查 $uploadOk 是否为 0
if ($uploadOk == 0) {
    echo "上传失败.";
// 如果一切顺利，尝试上传文件
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
        echo "文件 ". basename( $_FILES["fileToUpload"]["name"]). " 上传成功.";
    } else {
        echo "出现错误，文件未上传.";
    }
}
?>